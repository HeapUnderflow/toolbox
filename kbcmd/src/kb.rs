use std::path::{Path, PathBuf};
use tokio::process::Command;
use once_cell::sync::Lazy;
use std::env;
use std::io;

/// Check the following in order
///
/// - KEYBASE_BIN is set in the environment
/// - check path for `keybase` binary
///   - Linux/MacOS: ask which for a path
///   - Windows: attempt to execute `keybase.exe` directly
pub fn find_keybase() -> Option<PathBuf> {
	// this will *only* hand over to the executor on the first request and only if KEYBASE_BIN is not set
	static KB_BIN: Lazy<Option<PathBuf>> = Lazy::new(|| {
		if let Ok(v) = env::var("KEYBASE_BIN") {
			return Some(Path::new(v.trim()).to_path_buf());
		}

		#[cfg(not(windows))]
		async fn check_path() -> Option<String> {
			Some(
				String::from_utf8(Command::new("which").arg("keybase").output().await.ok()?.stdout)
					.expect("Malformed utf8"),
			)
		}

		#[cfg(windows)]
		async fn check_path() -> Option<String> {
			if Command::new("keybase.exe").arg("version").output().await.is_ok() {
				Some("keybase.exe".to_string())
			} else {
				None
			}
		}

		// TODO: use cfg_if

		if let Some(pth) = check_path().await {
			return Some(Path::new(pth.trim()).to_path_buf());
		}

		None
	});
	// check env

	KB_BIN.clone()
}

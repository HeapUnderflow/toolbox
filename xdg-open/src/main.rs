use regex::Regex;
use serde::{Deserialize, Serialize};
use std::{fs, process};

pub mod serde_regex;

#[derive(Debug, Serialize, Deserialize)]
struct Cfg {
	xdg_path: String,
	ropes:    Vec<Rope>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Rope {
	#[serde(with = "serde_regex")]
	test: Regex,
	run:  String,
}

macro_rules! exit_on_error {
	($e:expr,code: $c:expr) => {{
		match $e {
			Ok(o) => o,
			Err(e) => {
				eprintln!("ERROR: {:?}", e);
				return Err($c);
			},
			}
		}};
}

fn main() {
	match realmain() {
		Ok(()) => (),
		Err(e) => {
			process::exit(e);
		},
	}
}

fn realmain() -> Result<(), i32> {
	let pt = dirs::config_dir().expect("failed to discern config dir").join(env!("CARGO_PKG_NAME"));
	if !pt.exists() {
		exit_on_error!(fs::create_dir_all(&pt), code: 0xF1);
	}
	let pt = pt.join("config.yml");
	if !pt.exists() {
		let f = exit_on_error!(fs::File::create(&pt), code: 0xF2);
		exit_on_error!(serde_yaml::to_writer(f, &Cfg {
			xdg_path: std::env::var("HEAPXDG_DEFAULT_PATH").unwrap_or_else(|_| String::from("/usr/bin/xdg-open")),
			ropes: vec![
				Rope {
					test: Regex::new("^https?://.*").unwrap(),
					run: "firefox $1".to_owned()
				}
			]
		}), code: 0xF3);
	}

	let cfg_file = exit_on_error!(fs::File::open(&pt), code: 0xF4);
	let cfg: Cfg = exit_on_error!(serde_yaml::from_reader(cfg_file), code: 0xF5);

	if let Some(arg) = std::env::args().nth(1).map(|v| v.trim().to_owned()) {
		if let Some(mt) = cfg.ropes.iter().find(|v| v.test.is_match(&arg)) {
			let sp = exit_on_error!(shell_words::split(&mt.run), code: 0xF6)
				.into_iter()
				.map(|v| if v == "$1" { arg.clone() } else { v })
				.collect::<Vec<_>>();
			if sp.is_empty() {
				exit_on_error!(Err(format!("malformed run argument: {}", mt.run)), code: 0xF7);
			}

			let ex = exit_on_error!(exit_on_error!(process::Command::new(&sp[0]).args(&sp[1..]).spawn(), code: 0xF8).wait(), code: 0xF9);

			if !ex.success() {
				return Err(ex.code().unwrap_or(0xFC));
			} else {
				return Ok(());
			}
		}
	}

	let r = exit_on_error!(exit_on_error!(process::Command::new(&cfg.xdg_path)
		.args(&std::env::args().skip(1).collect::<Vec<_>>())
		.spawn(), code: 0xFA)
		.wait(), code: 0xFB);

	if !r.success() {
		Err(r.code().unwrap_or(0xFC))
	} else {
		Ok(())
	}
}

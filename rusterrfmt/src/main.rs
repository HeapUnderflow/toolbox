use std::io::Read;

/// A simple struct to handle 'indenting' and 'unindenting' in a semi-automated
/// way
struct IndentPrintln {
	count: usize,
	by:    usize,
	sigil: char,
}

impl IndentPrintln {
	/// create a new object, specifying the width of the indent (sigil * by) and
	/// the sigil used
	pub fn new(by: usize, sigil: char) -> IndentPrintln { IndentPrintln { count: 0, by, sigil } }

	/// shift the indentation by 1 to the right
	pub fn indent(&mut self) { self.count = self.count.saturating_add(1); }

	/// shift the indentation by 1 to the left
	pub fn unindent(&mut self) { self.count = self.count.saturating_sub(1); }

	/// get the string required to represent the current indentation
	#[inline]
	pub fn get_indent(&self) -> String { repeat(self.sigil, self.count * self.by) }
}

/// repeat a char given c times
#[inline]
fn repeat(s: char, c: usize) -> String {
	let mut st = String::with_capacity(c);
	for _ in 0..c {
		st.push(s)
	}
	st
}

/// clamp a value between a range
#[inline]
fn clamp2<T: PartialOrd>(v: T, l: T, r: T) -> T {
	if v > r {
		r
	} else if v < l {
		l
	} else {
		v
	}
}

fn main() {
	let mut idp = IndentPrintln::new(4, ' ');

	let mut buf = Vec::new();
	let _ = std::io::stdin().lock().read_to_end(&mut buf).expect("failed read");
	let v = String::from_utf8(buf).expect("failure").chars().collect::<Vec<_>>();

	let mut out = String::with_capacity(v.len());
	let mut cl_mode = false;
	for idx in 0..v.len() {
		// we do not want to take apart closures, as they would obscure the type hirachy
		if v[idx] == '[' {
			cl_mode = true;
			out.push('[');
			continue;
		}
		if v[idx] == ']' {
			cl_mode = false;
			out.push(']');
			continue;
		}

		if v[idx] == '<' {
			out.push('<');
			out.push('\n');
			idp.indent();
			out.push_str(&idp.get_indent());
			continue;
		}
		if v[idx] == '>' {
			out.push('\n');
			idp.unindent();
			out.push_str(&idp.get_indent());
			out.push('>');
			continue;
		}
		if v[idx] == ',' {
			out.push(',');
			if !cl_mode {
				out.push('\n');
				out.push_str(&idp.get_indent());
			}
			continue;
		}

		if v[idx] == ' ' && !cl_mode && v[clamp2(idx - 1, 0, v.len() - 1)] != ':' {
			continue;
		}
		out.push(v[idx]);
	}

	println!("{}", out);
}

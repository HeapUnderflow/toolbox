use clap::Clap;
use std::{
	env,
	fs,
	io,
	io::Write,
	path::{Path, PathBuf},
	process,
};
use once_cell::sync::Lazy;

#[derive(Clap, Debug)]
#[clap(version = env!("CARGO_PKG_VERSION"), author = env!("CARGO_PKG_AUTHORS"))]
struct Args {
	/// The path to serve
	path:      String,
	/// Address to bind to
	#[clap(short = "a", long = "address")]
	addr:      Option<String>,
	/// Port to bind to
	#[clap(short, long, default_value = "8080")]
	port:      u16,
	/// Worker process count
	#[clap(long = "worker", default_value = "4")]
	workers:   u16,
	/// Disable gzip compression
	#[clap(short = "g", long = "no-gzip")]
	no_gzip:   bool,
	/// Enable index generation
	#[clap(short = "i", long = "autoindex")]
	autoindex: bool,
}

fn get_tempdir() -> &'static PathBuf {
	static TMPD: Lazy<PathBuf> = Lazy::new(|| {
		let tempdir = env::var("TMPDIR").unwrap_or_else(|_| String::from("/tmp"));
		let aspath = Path::new(&tempdir);
		if !aspath.exists() {
			eprintln!("the specified tempdir \"{}\" does not seem to exist!", tempdir);
			panic!("the temporary directory could not be found");
		}

		let output = String::from_utf8(
			process::Command::new("mktemp")
				.args(&["-d", "-q", "-p", aspath.to_str().unwrap(), "ngx_svr_XXXXXXXXXXXXXXXXXXXX"])
				.output()
				.expect("c tmpdir ffailed")
				.stdout,
		)
			.expect("failed to parse tmpdir name correctly")
			.trim()
			.to_owned();


		PathBuf::from(output)
	});
	&TMPD
}

fn rm_tmp<P: AsRef<Path>>(p: P) -> io::Result<()> {
	process::Command::new("rm")
		.args(&["-R", "-f", p.as_ref().to_str().unwrap()])
		.spawn()?
		.wait()?;
	Ok(())
}

fn nginx_cmd(p: &str, extra_args: &[&str]) -> io::Result<process::ExitStatus> {
	Ok(process::Command::new("nginx").args(&["-p", &get_tempdir().to_string_lossy(), "-c", p]).args(extra_args).spawn()?.wait()?)
}

fn main() {
	match realmain() {
		Ok(_) => (),
		Err(code) => process::exit(code),
	}
}

fn clean_exit(code: i32, msg: std::fmt::Arguments) -> Result<(), i32> {
	eprintln!("{}", msg);
	if !env::var("NGINX_SERVE_NO_CLEANCFG").map(|one| one == "1").unwrap_or(false) {
		println!("cleaning up...");
		rm_tmp(get_tempdir()).expect("Failed to remove temporary files");
	}
	return Err(code);
}

fn realmain() -> Result<(), i32> {
	let args = Args::parse();

	let canon_path = match fs::canonicalize(&args.path) {
		Ok(path) => path.to_string_lossy().into_owned(),
		Err(why) => {
			return clean_exit(1, format_args!("failed to canonicalize path: {:?}", why))
		},
	};

	let pidfile = get_tempdir().join("nginx.pid");
	let config_file = get_tempdir().join("ngnx.conf");
	let favicon = get_tempdir().join("favicon.ico");

	let config_content = format!(
		r#"# NGINX CONFIG
worker_processes {workers};
pid {pidfile};

error_log /proc/self/fd/2;
daemon off;

events {{
    worker_connections 512;
}}

http {{
    include mime.types;
    default_type application/octet-stream;

    sendfile on;
    keepalive_timeout 65;
    tcp_nopush on;
    tcp_nodelay on;
    gzip {gzip};

    types_hash_max_size 2048;
    types_hash_bucket_size 128;

    error_log /proc/self/fd/2;
    access_log /proc/self/fd/2;

    server {{
        listen {listen_args_ipv4} default_server;

        root {root};
        index index.html index.htm;
        location / {{
            autoindex {autoindex};
        }}
        location = /favicon.ico {{
            alias {favicon};
        }}
    }}
}}
"#,
		workers = args.workers,
		pidfile = pidfile.to_string_lossy(),
		gzip = if args.no_gzip { "off" } else { "on" },
		autoindex = if args.autoindex { "on" } else { "off" },
		listen_args_ipv4 = {
			if let Some(addr) = args.addr {
				format!("{}:{}", addr, args.port)
			} else {
				format!("{}", args.port)
			}
		},
		root = canon_path,
		favicon = favicon.to_string_lossy()
	);

	{ // write mime.types
		let mut outfile = match fs::File::create(get_tempdir().join("mime.types")) {
			Ok(file) => file,
			Err(why) => {
				return clean_exit(
					1,
					format_args!("failed to create mime.types: {:?}", why),
				)
			},
		};

		match outfile.write_all(include_bytes!("mime.types")) {
			Ok(()) => (),
			Err(why) => {
				return clean_exit(
					1,
					format_args!("failed to write mime.types: {:?}", why),
				)
			},
		}
	}

	{ // write favicon
		let mut outfile = match fs::File::create(&favicon) {
			Ok(file) => file,
			Err(why) => {
				return clean_exit(
					1,
					format_args!("failed to create favicon.ico: {:?}", why),
				);
			},
		};

		match outfile.write_all(include_bytes!("favicon.ico")) {
			Ok(()) => (),
			Err(why) => {
				return clean_exit(
					1,
					format_args!("failed to create favicon.ico: {:?}", why),
				);
			},
		}
	}

	{ // write config file
		let mut outfile = match fs::File::create(&config_file) {
			Ok(file) => file,
			Err(why) => {
				return clean_exit(
					1,
					format_args!(
						"failed to create temporary config file to {}: {:?}",
						config_file.to_string_lossy(),
						why
					),
				)
			},
		};

		match outfile.write_all(&config_content.as_bytes()) {
			Ok(()) => (),
			Err(why) => {
				return clean_exit(
					1,
					format_args!("failed to write temporary config file: {:?}", why),
				)
			},
		}
	}

	{ // create forced elog
		match fs::create_dir(get_tempdir().join("logs")) {
			Ok(()) => (),
			Err(why) => {
				return clean_exit(
					1,
					format_args!("failed to create logs dir: {:?}", why))
			}
		}

		match fs::File::create(get_tempdir().join("logs").join("error.log")) {
			Ok(file) => drop(file),
			Err(why) => {
				return clean_exit(
					1,
					format_args!("failed to create logs error: {:?}", why))
			}
		}
	}

	let cfgf = match fs::canonicalize(config_file) {
		Ok(cfg) => cfg,
		Err(why) => {
			return clean_exit(
				1,
				format_args!("failed to canonicalize config path: {:?}", why),
			)
		},
	};

	let sp = cfgf.to_str().unwrap();

	if !nginx_cmd(sp, &["-t"])
		.map_err(|err| {
			eprintln!("nginx cmd errored: {:?}", err);
			1
		})?
		.success()
	{
		return clean_exit(
			2,
			format_args!(
				"nginx signaled that the config is not ok. please check your arguments and try \
				 again"
			),
		);
	}

	ctrlc::set_handler(move || {
		let f = match fs::read_to_string(&pidfile) {
			Ok(file) => file.trim().to_owned(),
			Err(e) => {
				eprintln!(
					"FATAL ERROR: failed to read pidfile {}: {:?}",
					&pidfile.to_string_lossy(),
					e
				);
				return;
			},
		};

		let sig = |signal: &str| -> io::Result<bool> {
			Ok(process::Command::new("kill").args(&["-s", signal, &f]).spawn()?.wait()?.success())
		};
		match sig("SIGTERM") {
			Ok(true) => (),
			Ok(false) | Err(_) => {
				sig("SIGKILL").expect("failed to kill nginx");
			},
		}

		// thread::sleep(Duration::from_millis(500));
		// let _ = tx.send(Message::Close);
	})
	.expect("failed to register panic handler");

	eprintln!("nginx serve started!");

	if !nginx_cmd(sp, &[])
		.map_err(|err| {
			eprintln!("nginx cmd errored: {:?}", err);
			1
		})?
		.success()
	{
		return clean_exit(1, format_args!("failed to start nginx"));
	}

	rm_tmp(get_tempdir()).expect("failed to remove temporary files");
	Ok(())
}

use pest::Parser;
use std::{io, io::Write};
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

#[derive(Parser)]
#[grammar = "parser.pest"]
struct ColorStringParser;

/// Convert a either 3 digit or 6 digit hex color code into r g b
fn rgb_from_hex(s: &str) -> Option<(u8, u8, u8)> {
	if s.len() == 6 {
		let r = u8::from_str_radix(&s[0..=1], 16).ok()?;
		let g = u8::from_str_radix(&s[2..=3], 16).ok()?;
		let b = u8::from_str_radix(&s[4..=5], 16).ok()?;
		Some((r, g, b))
	} else if s.len() == 3 {
		let r = u8::from_str_radix(&s[0..1], 16).ok()?;
		let g = u8::from_str_radix(&s[1..2], 16).ok()?;
		let b = u8::from_str_radix(&s[2..3], 16).ok()?;
		Some(((r << 4) + r, (g << 4) + g, (b << 4) + b))
	} else {
		None
	}
}

/// Parse the color from the color chosen
///
/// This can be either
/// 1. A named color
/// 2. A color code
fn parse_color_code(rr: pest::iterators::Pair<'_, Rule>) -> Color {
	let parse_rule = rr.into_inner().next().unwrap();

	match parse_rule.as_rule() {
		Rule::named => match parse_rule.as_str().to_ascii_lowercase().as_str() {
			"black" => Color::Black,
			"blue" => Color::Blue,
			"green" => Color::Green,
			"red" => Color::Red,
			"cyan" => Color::Cyan,
			"magenta" => Color::Magenta,
			"yellow" => Color::Yellow,
			"white" => Color::White,
			_ => Color::White,
		},
		Rule::code => match rgb_from_hex(&parse_rule.as_str()[1..]) {
			Some((r, g, b)) => Color::Rgb(r, g, b),
			None => Color::White,
		},
		v => unreachable!("rule = {:?}", v),
	}
}

/// Parse the format options from the tokens
fn parse_color_opt(r: pest::iterators::Pair<'_, Rule>) -> ColorSpec {
	let mut spec = ColorSpec::new();
	spec.set_reset(false);
	for v in r.into_inner() {
		match v.as_rule() {
			Rule::fg => {
				spec.set_fg(Some(parse_color_code(v.into_inner().next().unwrap())));
			},
			Rule::bg => {
				spec.set_bg(Some(parse_color_code(v.into_inner().next().unwrap())));
			},
			Rule::mode => match v.as_str().to_ascii_lowercase().as_str() {
				"italic" => {
					spec.set_italic(true);
				},
				"bold" => {
					spec.set_bold(true);
				},
				"underline" | "underlined" => {
					spec.set_underline(true);
				},
				"intense" => {
					spec.set_intense(true);
				},
				p => unreachable!("unknown mode = {}", p),
			},
			_ => unreachable!(),
		}
	}

	spec
}

/// Write a colored string to stdout. The grammar for this is located in
/// `src/parser.pest` using the Pest BNF
pub fn write_color(s: &str, cc: ColorChoice) -> io::Result<()> {
	if s.is_empty() {
		return Ok(());
	}
	let mut stdout = StandardStream::stdout(cc);
	let mut pairs = ColorStringParser::parse(Rule::text, s)
		.map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))?;

	let primary = pairs.next().unwrap();
	for pair in primary.into_inner() {
		match pair.as_rule() {
			Rule::string => write!(stdout, "{}", pair.as_str())?,
			Rule::format => {
				stdout.set_color(&parse_color_opt(pair))?;
			},
			Rule::reset => {
				stdout.set_color(ColorSpec::new().set_reset(true))?;
			},
			_ => unreachable!(),
		}
	}

	Ok(())
}

# -*- mode: makefile; coding: utf-8; -*-

# Build all the tools
all: colorize nginx_serve randstr rusterrfmt screenshot xdg-open-proxy kbcmd

# Keybase chat commands
kbcmd:
	cargo build --release --bin kbcmd

# Tool to emit color escape sequences in a human friendly way
colorize:
	cargo build --release --bin colorize

# Serve anything by abusing nginx
nginx_serve:
	cargo build --release --bin nginx_serve

# Generate a random string with a (somewhat) secure PRNG
randstr:
	cargo build --release --bin randstr

# A naive tool to make long rust types more readable
rusterrfmt:
	cargo build --release --bin rusterrfmt

# Utility to ease the creation of screenshots via hotkey
screenshot:
	cargo build --release --bin screenshot

# A xdg-open proxy to avoid having to hit the database
xdg-open-proxy:
	cargo build --release --bin xdg-open-proxy

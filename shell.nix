{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    binutils gcc gnumake openssl pkgconfig cacert
  ];
}

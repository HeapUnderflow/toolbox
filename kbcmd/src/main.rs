pub mod config;
// pub mod kb;
// pub mod parser;
// pub mod runner;

#[tokio::main]
async fn main() {
	// set up tracing (log)
	tracing_subscriber::fmt::init();

	// setup span
	let _span = tracing::debug_span!("setup");
	let _guard = _span.enter();

	let cfg = match config::Config::load_from_file("config.json") {
		Ok(Some(cfg)) => cfg,
		Ok(None) => {
			tracing::warn!("config was missing, created new");
			return;
		},
		Err(e) => {
			tracing::error!(error = ?e, "failed to load config");
			return;
		},
	};

	// drop the setup span guard
	drop(_guard);

	// tracing::debug!(kb_bin = ?kb::find_keybase().await);
}

# Heaps Toolbox

## randstr

```
randstr 1.0.0
HeapUnderflow <heapunderflow@outlook.com>
Generate a random string using an alphabet

USAGE:
    randstr [FLAGS] [OPTIONS]

FLAGS:
    -h, --help          Prints help information
    -n, --no-newline    Do not emit a newline (0x0A) after the last byte written
    -V, --version       Prints version information

OPTIONS:
    -a, --alphabet <alphabet>    Alphabet to use, default is a-zA-Z0-9. This ONLY supports single-byte sequences (for
                                 now)
    -l, --length <length>        Length of the string generated [default: 25]
```

#### TODOS

- Allow use of ranges in alphabet: `[a-f]XYZ` instead of `abcdefXYZ`
- Allow use of hex values: `\x00`

## colorize

```
colorize 1.0.0
HeapUnderflow <heapunderflow@outlook.com>
Read in a string from stdin, and transform it to a colored output

USAGE:
    colorize [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --color <color>     [default: auto]  [possible values: always, auto, never]
```

#### TODOS

- Allow specifiying the string as argument(s) to the binary
- Allow "escaping" a format specifyer
- Better docs for the format specifier

## keybase-run-chat-commands

Run system commands directly from your Keybase Chat.

This was *heavily* inspired by [this script](https://keybase.pub/heapunderflow/scripts/run-chat-commands.sh) from [7roxel](https://keybase.io/7roxel)

#### TODOS

- Add CommandLine args
- See more todos in [main.rs](keybase-run-chat-commands/src/main.rs)

## screenshot

Utility to ease the taking of screenshot over a hotkey (all the defaults are supplied in the config file)

```
screenshot 1.0.0
HeapUnderflow <heapunderflow@outlook.com>
Screenshot tool proxy

USAGE:
    screenshot [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --config <config>    Config File, defaults to $XDG_CONFIG_HOME/screenshot/config.json
    -o, --out <out>          Write file to this path instead of the clipboard
```

Config File:

```json
{
  "notify": [
    "notify-send",
    "-u",
    "low",
    "-a",
    "maim",
    "$header",
    "$message"
  ],
  "screenshot": [
    "maim",
    "-f",
    "png",
    "-m",
    "10",
    "-su"
  ],
  "clipboard": [
    "xclip",
    "-selection",
    "clipboard",
    "-t",
    "image/png"
  ]
}
```

#### TODOS

None (for now)

## xdg-open

A super thin wrapper over the actual xdg-open to facilitate the intercepting of 
files and/or websites without having to fiddle with xdg-opens database (that i do not properly understand)

The config for it looks like this

```yml
---
xdg_path: "/usr/bin/xdg-open"
ropes:
  - test: "^https?://.*"
    run: firefox $1
```

where test is the regex used to check if a option applies, and run the command to run.
`$1` is replaced with the xdg-open argument. All arguments are checked in order specified
and it will stop searching after the first match.

If there is no match the arguments are forwarded to the normal xdg-open as-is.

#### TODOS

None (for now)
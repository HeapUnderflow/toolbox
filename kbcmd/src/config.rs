use serde::{Deserialize, Serialize};
use std::{fs::File, io, path::Path};
use tokio::sync::RwLock;

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
	pub owner:         String,
	pub allowed_users: Vec<String>,
}

impl Default for Config {
	fn default() -> Self { Self { owner: String::default(), allowed_users: Vec::default() } }
}

impl Config {
	pub fn new(owner: String, allowed_users: Vec<String>) -> Self {
		Config { owner, allowed_users }
	}

	#[tracing::instrument]
	pub fn load_from_file(path: &str) -> io::Result<Option<Config>> {
		if !Path::new(path).exists() {
			let newcfg = Config::default();

			tracing::debug!(new = ?newcfg, "config not found, recreating");

			let f = File::create(path)?;
			serde_json::to_writer_pretty(f, &newcfg)
				.map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;
			return Ok(None);
		}

		let f = File::open(path)?;
		let data: Config =
			serde_json::from_reader(f).map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;
		tracing::trace!("loaded config");
		Ok(Some(data))
	}
}

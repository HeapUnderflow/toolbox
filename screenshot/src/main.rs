use clap::Clap;
use process::Command;
use serde::{Deserialize, Serialize};
use std::{fs, io, io::Write, process};

macro_rules! exit_on_error {
	($e:expr, $c:expr) => {{
		match $e {
			Ok(o) => o,
			Err(e) => {
				eprintln!("ERROR: {:?}", e);
				return Err($c);
			},
			}
		}};
}

/// Screenshot tool proxy
#[derive(Debug, Clap)]
#[clap(version = env!("CARGO_PKG_VERSION"), author = env!("CARGO_PKG_AUTHORS"))]
struct Args {
	/// Write file to this path instead of the clipboard
	#[clap(short = "o", long = "out")]
	out: Option<String>,

	/// Config File, defaults to $XDG_CONFIG_HOME/screenshot/config.json
	#[clap(short = "c", long = "config")]
	config: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Opt {
	notify:     Vec<String>,
	screenshot: Vec<String>,
	clipboard:  Vec<String>,
}

impl Default for Opt {
	fn default() -> Self {
		Opt {
			notify:     vec!["notify-send", "-u", "low", "-a", "maim", "$header", "$message"]
				.into_iter()
				.map(|v| v.to_owned())
				.collect(),
			screenshot: vec!["maim", "-f", "png", "-m", "10", "-su"]
				.into_iter()
				.map(|v| v.to_owned())
				.collect(),
			clipboard:  vec!["xclip", "-selection", "clipboard", "-t", "image/png"]
				.into_iter()
				.map(|v| v.to_owned())
				.collect(),
		}
	}
}

fn main() {
	match realmain() {
		Ok(()) => (),
		Err(e) => {
			process::exit(e);
		},
	}
}

fn realmain() -> Result<(), i32> {
	let args = Args::parse();
	let cfg: Opt = if let Some(ref out) = args.config {
		let cfg_file = exit_on_error!(fs::File::open(&out), 0xF4);
		exit_on_error!(serde_json::from_reader(cfg_file), 0xF5)
	} else {
		let pt =
			dirs::config_dir().expect("failed to discern config dir").join(env!("CARGO_PKG_NAME"));
		if !pt.exists() {
			exit_on_error!(fs::create_dir_all(&pt), 0xF1);
		}
		let pt = pt.join("config.json");
		if !pt.exists() {
			let f = exit_on_error!(fs::File::create(&pt), 0xF2);
			exit_on_error!(serde_json::to_writer_pretty(f, &Opt::default()), 0xF3);
		}

		let cfg_file = exit_on_error!(fs::File::open(&pt), 0xF4);
		exit_on_error!(serde_json::from_reader(cfg_file), 0xF5)
	};

	let screenshot = match screenshot(&cfg.screenshot) {
		Ok(sc) => sc,
		Err(e) => {
			eprintln!("Failed to create screenshot: {:?}", e);
			return Err(0xE2);
		},
	};

	if screenshot.is_empty() {
		if let Err(e) = notify(&cfg.notify, "Image Capture Aborted", "Aborted screenshot") {
			eprintln!("Failed to send notification: {:?}", e);
			return Err(0xE9);
		} else {
			return Ok(());
		}
	}

	if let Some(ref fname) = args.out {
		let mut f = match fs::File::create(fname) {
			Ok(file) => file,
			Err(e) => {
				eprintln!("failed to create output file {}: {:?}", fname, e);
				return Err(0xEA);
			},
		};

		if let Err(e) = f.write_all(&screenshot) {
			eprintln!("failed to write to output file: {:?}", e);
			return Err(0xEB);
		}
	} else if let Err(e) = clipboard(&cfg.clipboard, &screenshot) {
		eprintln!("Failed to copy screenshot to clipboard: {:?}", e);
		return Err(0xE3);
	}

	let message = match args.out {
		Some(out) => format!("Sucessfully saved screenshot to {}", out),
		None => String::from("Sucessfully captured screenshot"),
	};

	if let Err(e) = notify(&cfg.notify, "Image Caputed", &message) {
		eprintln!("Failed to send notification: {:?}", e);
		return Err(0xE4);
	}

	Ok(())
}

#[inline]
fn screenshot(command: &[String]) -> io::Result<Vec<u8>> {
	debug_assert!(!command.is_empty());
	Command::new(&command[0]).args(&command[1..]).output().map(|v| v.stdout)
}

fn notify(command: &[String], header: &str, message: &str) -> io::Result<()> {
	debug_assert!(!command.is_empty());
	let args = command[1..]
		.iter()
		.map(|v| if *v == "$header" { header } else { v })
		.map(|v| if v == "$message" { message } else { v })
		.collect::<Vec<_>>();

	Command::new(&command[0]).args(&args).spawn()?.wait().and_then(|v| {
		if v.success() {
			Ok(())
		} else {
			Err(io::Error::new(
				io::ErrorKind::Other,
				format!("result code: {}", v.code().unwrap_or(1)),
			))
		}
	})
}

fn clipboard(command: &[String], data: &[u8]) -> io::Result<()> {
	debug_assert!(!command.is_empty());
	let mut child =
		Command::new(&command[0]).args(&command[1..]).stdin(process::Stdio::piped()).spawn()?;
	{
		let stdin = match child.stdin.as_mut() {
			Some(sin) => sin,
			None => {
				return Err(io::Error::new(
					io::ErrorKind::Other,
					String::from("failed to open stdin of child process"),
				));
			},
		};

		stdin.write_all(data)?;
	}

	child.wait().and_then(|v| {
		if v.success() {
			Ok(())
		} else {
			Err(io::Error::new(
				io::ErrorKind::Other,
				format!("result code: {}", v.code().unwrap_or(1)),
			))
		}
	})
}

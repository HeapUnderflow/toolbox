#[macro_use]
extern crate pest_derive;

use clap::Clap;
use std::{
	io,
	io::{BufRead, BufReader},
};
use termcolor::ColorChoice;

mod parser;

/// Read in a string from stdin, and transform it to a
#[derive(Clap)]
#[clap(author = env!("CARGO_PKG_AUTHORS"), version = env!("CARGO_PKG_VERSION"))]
struct Args {
	/// Set the color mode
	#[clap(short = "c", long = "color", takes_value = true, possible_values = &["always", "auto", "never"], default_value = "auto")]
	color: String,

	/// Add a final newline to the output
	#[clap(short = "n", long = "newline")]
	newline: bool,

	/// Use text given by "text" instead of reading from stdin.
	/// This reads all the text given at once into memory. if you rely on things in a streaming fashion use stdin
	#[clap(short = "t", long = "text", takes_value = true)]
	text: Option<String>
}

fn main() {
	let args = Args::parse();
	let stdin = io::stdin();

	let read: Box<dyn BufRead> = match args.text {
		Some(text) => Box::new(BufReader::new(io::Cursor::new(text))),
		None => Box::new(BufReader::new(stdin.lock()))
	};

	let cc = match args.color.as_str() {
		"always" => ColorChoice::Always,
		"never" => ColorChoice::Never,
		"auto" if atty::is(atty::Stream::Stdout) => ColorChoice::Auto,
		"auto" => ColorChoice::Never,
		s => unreachable!("the option {} should never be valid", s),
	};

	let mut lines = read.lines().peekable();
	loop {
		let ln = match lines.next() {
			Some(line) => line,
			None => break,
		};

		match ln {
			Ok(line) => {
				if let Err(e) = parser::write_color(&line, cc) {
					eprintln!("failed to write to stdout: {:?}", e);
					std::process::exit(1);
				}
				if lines.peek().is_some() || args.newline {
					println!();
				}
			},
			Err(e) => {
				eprintln!("failed to read from stdin: {:?}", e);
				std::process::exit(1);
			},
		}
	}
}

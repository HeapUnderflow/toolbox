use pest::{iterators::Pair, Parser};
use std::io;

#[derive(Parser)]
#[grammar = "alph.pest"]
struct AlphabetParser;

pub fn parse_alphabet(s: &str) -> io::Result<Vec<u8>> {
	let mut tokens = AlphabetParser::parse(Rule::alphabet, s)
		.map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))?;

	let primary = tokens.next().unwrap();

	let mut data = Vec::new();

	for item in primary.into_inner() {
		match item.as_rule() {
			Rule::char => data.push(item.as_str().bytes().next().unwrap()),
			Rule::hex => match u8::from_str_radix(item.as_str(), 16) {
				Ok(val) => data.push(val),
				Err(e) => eprintln!("unable to parse hex value {}: {:?}", item.as_str(), e),
			},
			Rule::range => parse_range(item, &mut data),
			item => unreachable!("reached unreachable item: {:?}", item),
		}
	}

	Ok(data)
}

fn parse_range(pair: Pair<'_, Rule>, out: &mut Vec<u8>) {
	for item in pair.into_inner() {
		let mut inner = item.into_inner();

		let left = inner.next().unwrap().as_str().bytes().next().unwrap();
		let right = inner.next().unwrap().as_str().bytes().next().unwrap();

		let sr = order(left, right);
		for i in sr.0..=sr.1 {
			out.push(i);
		}
	}
}

fn order<T>(l: T, r: T) -> (T, T)
where
	T: PartialOrd,
{
	if r < l {
		(r, l)
	} else {
		(l, r)
	}
}

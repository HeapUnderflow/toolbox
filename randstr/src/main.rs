#[macro_use]
extern crate pest_derive;

use clap::Clap;
use rand::{rngs::OsRng, RngCore};
use std::{io, io::Write};

mod parser;

/// Generate a random string using an alphabet
#[derive(Debug, Clap)]
#[clap(author = env!("CARGO_PKG_AUTHORS"), version = env!("CARGO_PKG_VERSION"))]
struct Args {
	/// Length of the string generated
	#[clap(short = "l", long = "length", default_value = "25")]
	length:     usize,
	/// Alphabet to use
	#[clap(short = "a", long = "alphabet", default_value = "[a-zA-Z0-9]")]
	alphabet:   String,
	/// Do not emit a newline (0x0A) after the last byte written
	#[clap(short = "n", long = "no-newline")]
	no_newline: bool,
}

fn main() -> io::Result<()> {
	let args = Args::parse();

	let mut buf = [0u8; 512];
	let mut generated = 0;
	let mut rng = OsRng;

	let alpha = parser::parse_alphabet(&args.alphabet)?;

	let stdout = io::stdout();
	let mut stdout_lock = stdout.lock();
	'work: loop {
		rng.fill_bytes(&mut buf);
		for chr in buf.iter().filter(|b| alpha.iter().any(|a| a == *b)) {
			stdout_lock.write_all(&[*chr])?;
			generated += 1;
			if generated >= args.length {
				break 'work;
			}
		}
	}

	if !args.no_newline {
		stdout_lock.write_all(&[b'\n'])?;
	}

	Ok(())
}
